//Tianheng Xiang CSE002 3/26/2018 Lab07 Methods
import java.util.Random;
import java.util.Scanner;
public class lab07{
public static void main(String args[]){
  Scanner myScanner = new Scanner(System.in);
//   System.out.println("Would you like more sentence? If so type 1, if no type zero.");
//   int ifOrNot = myScanner.nextInt();

  for (int i = 0; i < 5; i++){
    String adjective = Adjectives();
    String nounSubject = subjectNoun();
    String nounObject = objectNoun();
    String vern = pastTenseVerbs();
    String adjective1 =Adjectives();
    System.out.println("The" + adjective + adjective1 + nounSubject + "the" + vern + nounObject);
  }

  }


public static String Adjectives(){
  int randomInt = randomInt();
  String output = "";
  switch(randomInt) {
    case 1:  output = " attractive ";
             break;
    case 2:  output = " bald ";
             break;
    case 3:  output = " beautiful ";
             break;
    case 4:  output = " elegant ";
             break;
    case 5:  output = " glamorous ";
             break;
    case 6:  output = " magnificent ";
             break;
    case 7:  output = " plump ";
             break;
    case 8:  output = " scruffy ";
             break;
    case 9:  output = " unkempt ";
             break;
  }
  return output;
}
public static String subjectNoun(){
  int randomInt = randomInt();
  String output = "";
  switch(randomInt) {
    case 1:  output = " people ";
             break;
    case 2:  output = " history ";
             break;
    case 3:  output = " way ";
             break;
    case 4:  output = " art ";
             break;
    case 5:  output = " world ";
             break;
    case 6:  output = " information ";
             break;
    case 7:  output = " map ";
             break;
    case 8:  output = " family ";
             break;
    case 9:  output = " government ";
             break;
  }
  return output;
}

public static String objectNoun(){
  int randomInt = randomInt();
  String output = "";
  switch(randomInt) {
    case 1:  output = " health ";
             break;
    case 2:  output = " system ";
             break;
    case 3:  output = " computer ";
             break;
    case 4:  output = " meat ";
             break;
    case 5:  output = " year ";
             break;
    case 6:  output = " music ";
             break;
    case 7:  output = " reading ";
             break;
    case 8:  output = " method ";
             break;
    case 9:  output = " data ";
             break;
  }
  return output;
}

public static String pastTenseVerbs(){
  int randomInt = randomInt();
  String output = "";
  switch(randomInt) {
    case 1:  output = " sang ";
             break;
    case 2:  output = " became ";
             break;
    case 3:  output = " began ";
             break;
    case 4:  output = " bent ";
             break;
    case 5:  output = " blew ";
             break;
    case 6:  output = " broke ";
             break;
    case 7:  output = " built ";
             break;
    case 8:  output = " bought ";
             break;
    case 9:  output = " caught ";
             break;
  }
  return output;
}

public static int randomInt(){
  Random randomGenerator = new Random();
  int randomInt = randomGenerator.nextInt(10);
  return randomInt;
}
}