// Tianheng Xiang CSE002 Remove Elements
import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = testInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
	public static int[] randomInput(){
		int []numbers=new int[10];
		for(int i = 0; i<10; i++){
			numbers[i]=randomInt();
		}
		return numbers;
	}
	public static int[] delete(int[] num, int index){
		for ( int i = index; i < num.length -1 ; i++){
			num[i]=num[i+1];
		}
		return num;
	}
	public static int[] remove(int[] num, int target){
		for (int i = 0; i < num.length - 1; i++){
			if (num[i]==target){
				num[i]=num[i+1];
				System.out.println("Element "+ target+" has been found.");
			}
			else{
				System.out.println("Element "+ target+" was not found.");
			}
		}
		return num;
	}
	public static int randomInt(){
  Random randomGenerator = new Random();
  int randomInt = randomGenerator.nextInt(10);
  return randomInt;
	}
	  public static int testInt(){//testify the input is int
    boolean isInt = false;
    int a = 0;
    Scanner myScanner = new Scanner (System.in);
    while (isInt==false){
     System.out.println("Enter an int! ");
     isInt= myScanner.hasNextInt();
     if (isInt==true){
       a = myScanner.nextInt();
     }
     else{
         String junkWord = myScanner.next(); 
     } 
       if(a>=0 && a < 10){
        }                           
       else{
        isInt=false;
        System.out.println("Enter an int in the range of 0-10!");
       }
   }
      return a;
  }
}



