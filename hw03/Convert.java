//Tianheng Xiang 2/10/2018 CSE002 hw03 Convert//
//This document works as a converter to convert the quantity of rain from inches dropped into cubic miles.
import java.util.Scanner;
public class Convert{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);//declare the instance of Scanner and named it as myScanner
    System.out.print("Enter the affected area in acres in the form of xx.xx:");//prompt the user for entering the affected area in acres
    double affectedAreaInAcre = myScanner.nextDouble();
    System.out.print("Enter the rainfall in the affect area in inches in the form of xx:");//prompt the user for entering the amount of rainfall per area of the affect area
    double rainfallAmountInInch = myScanner.nextInt();
    
   double rainfallAmountInFt = rainfallAmountInInch / 12;//convert the rainfall amount in inch into ft
    double rainQuantityInAcreFt = affectedAreaInAcre * rainfallAmountInFt;//generate the value of volume of rain in Acre*Ft by multipling value of affected area in acre and rain fall amount in feet
    double rainQuantityInGallon = rainQuantityInAcreFt * 325851;//convert the rainfall amount in Acre into gallon
    double rainQuantityInCubicMiles = rainQuantityInGallon * (9.08169E-13);//convert the rainfall amount in gallon into CubicMile
      
    System.out.println(rainQuantityInCubicMiles + " cubic miles");//display the value of rain quantity in mile
  }
  
  
  
}