//Tianheng Xiang 2/9/2018 CSE002 Pyramid
//This document can prompt the user for the dimension a pyramid and generate the volume inside the pyramid.
import java.util.Scanner;
public class Pyramid{
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);//declare the instance of Scanner and named it as myScanner
    System.out.print("The square side of the pyramid is (input length):");//prompt the user for entering the square side of the pyramid
    double squareSide = myScanner.nextDouble();
    System.out.print("The height of the pyramid is (input height):");//prompt the user for entering the height of the pyramid
    double height = myScanner.nextDouble();
    double pyramidVolume = Math.pow(squareSide, 2 ) * height / 3;//calculate the volume of the pyramid by formula V=(a*h)/3 which in turn "a"would be the square of the square side
    System.out.println("The volume inside the pyramid is: " + pyramidVolume);//display the volume inside the pyramid as calculated
  }
  
  
  
}