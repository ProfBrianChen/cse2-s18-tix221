//CSE002 Tianheng Xiang 3/20/2018 This document will generate argyle pattern
import java.util.Scanner;
public class Argyle{
  public static void main(String[] args) {
      Scanner myScanner;
    myScanner = new Scanner (System.in);
    boolean isWVInt= false;
    int widthView=0;
    
    boolean isHVInt=false;
    int heightView=0;
    
    boolean isWDInt= false;
    int widthDiamond=0;
    boolean isWSInt= false;
    int widthStripe=0;
    
	
	while (isWVInt==false){
    System.out.println("Enter a positive integer for the width of Viewing window in characters ");
    isWVInt= myScanner.hasNextInt();
    if (isWVInt==true){
      widthView= myScanner.nextInt();
    }
    else{
        String junkWord = myScanner.next(); 
    } 
      if(widthView>0){
        }                           
      else{
        isWVInt=false;
      }
  } 


/* Height of viewing window */
	
	  while (isHVInt==false){
    System.out.println("Enter a positive integer for the height of Viewing window in characters ");
    isHVInt= myScanner.hasNextInt();
    if (isHVInt==true){
      heightView= myScanner.nextInt();
    }
    else{
        String junkWord = myScanner.next();   
    } 
      if(heightView>0){
        }                            
      else{
        isHVInt=false;
      }
  }  	
	
	
/* width of the argyle diamonds */
	
	  while (isWDInt==false){
    System.out.println("Enter a positive integer for the width of the argyle diamonds ");
    isWDInt= myScanner.hasNextInt();
    if (isWDInt==true){
      widthDiamond= myScanner.nextInt();
    }
    else{
        String junkWord = myScanner.next();     
    } 
      if(widthDiamond>0){
        }                               
      else{
        isWDInt=false;
      }
  } 	
	

/* width of the argyle center stripe */

	  while (isWSInt==false){
    System.out.println("Enter a positive odd integer for the width of the argyle center stripe ");
    isWSInt= myScanner.hasNextInt();
    if (isWSInt==true){
      widthStripe= myScanner.nextInt();
    }
    else{
        String junkWord = myScanner.next();  
    } 
      if(widthStripe>0 && widthStripe<widthDiamond/2 && widthStripe%2==1){}
			else{
        isWSInt=false;
      }
  }
		
	  System.out.print("Enter the first character for the pattern fill ");
    String string1 = myScanner.next();
	  char pattern1 = string1.charAt(0);
		
    System.out.print("Enter the second character for the pattern fill ");
    String string2 = myScanner.next();
	  char pattern2 = string2.charAt(0); 
		
    System.out.print("Enter the third character for the pattern fill ");
    String string3 = myScanner.next();
	  char pattern3 = string3.charAt(0); 	
    
		
		/* Plot the graph */
        for (int i = 0; i < (heightView); i++){   
            for(int j = 0; j < (widthView); j++){   
              if ( (j % (widthDiamond*2)) <= i % (widthDiamond*2) + widthStripe/2 && (j % (widthDiamond*2)) >= i % (widthDiamond*2) - widthStripe / 2){
                System.out.print(pattern3);
              } else if ( (j % (widthDiamond*2)) <= widthDiamond*2 - i % (widthDiamond*2) - 1 + widthStripe / 2 && (j % (widthDiamond*2)) >= widthDiamond * 2 - i % (widthDiamond*2) - 1 -widthStripe / 2 ) {
                System.out.print(pattern3);
              } else if ( (j % (widthDiamond*2)) <= widthDiamond + (i % (widthDiamond*2)) % widthDiamond - 1 && (j % (widthDiamond*2)) > widthDiamond - (i % (widthDiamond*2)) % widthDiamond - 1 && (i % (widthDiamond*2)) < widthDiamond ){
                System.out.print(pattern2);
              } else if ( (j % (widthDiamond*2)) >= (i % (widthDiamond*2)) % widthDiamond && (j % (widthDiamond*2)) <= widthDiamond *2 - (i % (widthDiamond*2)) % widthDiamond - 1 && (i % (widthDiamond*2)) >= widthDiamond ) {
                System.out.print(pattern2);
              } else {
                System.out.print(pattern1);
              }
    }
                System.out.println();
    }
  }
}
