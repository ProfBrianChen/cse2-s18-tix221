//Tianheng Xiang 2/9/2018 CSE002 lab03 This document aims to use Scanner class to evenly split the check. The origin cost of check, percentage tip and number of ways the check to be split will be the input, then display how much each person will need to pay.
import java.util.Scanner;
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
      Scanner myScanner = new Scanner( System.in );//declare an instance of the Scanner object and call the Scanner constructor
      System.out.print("Enter the original cost of the check in the form xx.xx: ");//prompt the user for the original cost of the check
      double checkCost = myScanner.nextDouble();//accept the input and declare variable checkCost and assign this with value of the original cost of the check
      System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );//prompt the user for the tip percentage that they wish to pay
      double tipPercent = myScanner.nextDouble();//accept the input and declare variable tipPercent and assign this with value of the  tip percentage that consumers wish to pay
      tipPercent /= 100; //convert the percentage into a decimal value
      System.out.print("Enter the number of people who went out to dinner: ");//prompt the user for the number of people that went to dinner
      int numPeople = myScanner.nextInt();////accept the input and declare variable numPeople and assign this with value of the number of consumers go to dinner
/////////////////////////////////////////input ends here,output starts below//////////////////////////////////////////
      double totalCost;//declare variable as double: total cost of the dinner with tips
      double costPerPerson;//declare variable as double: cost per person
      int dollars,   //whole dollar amount of cost 
          dimes, pennies; //for storing digits
        //to the right of the decimal point 
        //for the cost$ 
      totalCost = checkCost * (1 + tipPercent);//calculate the cost with tax by multipling the cost of check with (tip percent+1) 
      costPerPerson = totalCost / numPeople;//calculate the cost per person by dividing the total cost with tips by number of people
      //get the whole amount, dropping decimal fraction
      dollars=(int)costPerPerson;//give out the number of dollar by truncating decimal
      //get dimes amount, e.g., 
      // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
      //  where the % (mod) operator returns the remainder
      //  after the division:   583%100 -> 83, 27%5 -> 2 
      dimes=(int)(costPerPerson * 10) % 10;//determine the value of dime
      pennies=(int)(costPerPerson * 100) % 10;//determine the value of penny
      System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);//display the value of money of each person need to pay


}  //end of main method   
  	} //end of class
