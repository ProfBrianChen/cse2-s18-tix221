//3/27/2018 CSE002 Area.java Tianheng Xiang
//This file will calculate the area of rectangle or triangle or circle with the values of dimensions
import java.util.Scanner;
import java.math.*;
public class Area {
  public static void main(String[] args) {
    String object = "";
    String shape = testEquality(object);
    double triangleBase = 0;
    double triangleHeight = 0;
    double circleRadius = 0;
    double rectangleLength = 0;
    double rectangleWidth = 0;
    double area = 0;
    if( shape.equals("triangle") ){
      System.out.println("Type the length of the base ");
      triangleBase = testDouble(triangleBase);
      System.out.println("Type the length of the height ");
      triangleHeight = testDouble(triangleHeight);
      area = areaTriangle(triangleBase,triangleHeight);
    }
    if( shape.equals("circle") ){
      System.out.println("Type the radius ");
      circleRadius = testDouble(circleRadius);
      area = areaCircle(circleRadius);
    }
    if( shape.equals("rectangle") ){
      System.out.println("Type the length of the rectangle ");
      rectangleLength = testDouble(rectangleLength);
      System.out.println("Type the width of the rectangle ");
      rectangleWidth = testDouble(rectangleWidth);
      area = areaRectangle(rectangleLength,rectangleWidth);
    }
    System.out.println("The area of " + object + " is " + area );
  }
  
  public static String testEquality(String shapeInput){
    Scanner myScanner = new Scanner (System.in);
    System.out.println("Type the shape you want in rectangle or triangle or circle. ");
    shapeInput = myScanner.next();
    while (!shapeInput.equals("triangle") && !shapeInput.equals("rectangle") && !shapeInput.equals("circle")){
      System.out.println("Invalid input! Type the shape you want in rectangle or triangle or circle. ");
      shapeInput = myScanner.next();
    }
    return shapeInput;
   }
  public static double testDouble(double doubleTest){//testify the input is double
    boolean isDouble = false;
    doubleTest = 0;
    Scanner myScanner = new Scanner (System.in);
    while (isDouble==false){
     System.out.println("Enter a double! ");
     isDouble= myScanner.hasNextDouble();
     if (isDouble==true){
       doubleTest = myScanner.nextDouble();
     }
     else{
         String junkWord = myScanner.next(); 
     } 
       if(doubleTest>0){
        }                           
       else{
        isDouble=false;
       }
   }
    return doubleTest;
  }
  public static double areaTriangle(double base, double Height) {//calculate the area of triangle
    return base*Height/2;
  }
  public static double areaCircle(double radius){//calculate the area of circle
    return radius*radius*(Math.PI);
  }
  public static double areaRectangle(double length, double width){//calculate the area of rectangle
    return length*width;
  }
}
