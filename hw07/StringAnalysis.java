//3/27/2018 CSE002 StringAnalysis.java Tianheng Xiang
//This programme will determine whether a string contains characters in all or some places of that string
import java.util.Scanner;
public class StringAnalysis {
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Type the string you want to analyze: ");
    String target = myScanner.next();
    int length = target.length();
    int number = 0;//the number user want to analyze
    boolean allAreLetters = false;
    System.out.print("How many number of string you want to analyze? ");
    
    boolean isInt = false;
    while (isInt == false){ //test whether the input number is an int
      System.out.println("Please type an integer! ");
      isInt = myScanner.hasNextInt();
      if(isInt == true){
        number = myScanner.nextInt();
      }
      else{
        String junkWord = myScanner.next();
      }
      if(number > 0){}
      else {
        isInt = false;
      }
    }
    
    if (number > length){
      allAreLetters = inputAnalyze(target);
    }
    else if (number < length){
      allAreLetters = inputAnalyze(target,number);
    }
    if (allAreLetters){
    System.out.println("All characters in string are letter.");
    }
    if (!allAreLetters){
    System.out.println("Some characters in string are not letter.");
    }
  }
  public static boolean inputAnalyze(String object){//testify whether the whole string contains letter
    int length = object.length();
    boolean letterOrNot = false;
    for (int i = 1; i < length+1; i++ ){
      if (Character.isLetter(object.charAt(i))){
        letterOrNot = true;
      }
      else{
        letterOrNot = true;
      }
    }
    return letterOrNot;
  }
  public static boolean inputAnalyze(String object, int specifiedNumber){//testify whether some characters of the string contain letter
    boolean letterOrNot = false;
    for (int i = 1; i < specifiedNumber+1; i++ ){
      if (Character.isLetter(object.charAt(i))){
        letterOrNot = true;
      }
      else{
        letterOrNot = false;
      }
    }
    return letterOrNot;
  }
  
}





