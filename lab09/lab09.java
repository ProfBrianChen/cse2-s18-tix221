//lab09
public class lab09{
  public static void main(String args[]){
    int[] array0 = new int[10];
    for(int i = 0; i < 10; i++){
      array0[i]=i;
    }
    int[] array1 = new int[10];
    int[] array3 = new int[10];
    array1 = copy(array0);
    int[] array2 = new int[10];
    array2 = copy(array0);
    inverter(array0);
    print(array0);
    System.out.println("");
    inverter2(array1);
    print(array1);
    System.out.println("");
    array3 = inverter2(array2);
    print(array3);
  }
  public static int[] copy(int[] list){
    int[] list1 = new int[list.length];
    for(int i=0; i<list.length; i++){
      list1[i]=list[i];
    }
    return list1;
  }
  public static void inverter(int[] list){
    int[] list1 = new int[list.length];
    for(int i = 0; i < list.length; i++){
      list1[i]=list[list.length-1-i];//In this part the length for the RHS should be list.length-i-1. To be cautious that (-1) provide the last aside to be 0.
    }
    for(int i = 0; i < list.length; i++){
      list[i]=list1[i];
    }
  }
  public static int[] inverter2(int[] list){
    int[] list1 = new int[list.length];
    list1 = copy(list);
    for(int i = 0; i < list.length; i++){
      list1[i]=list[list.length-1-i];//In this part the length for the RHS should be list.length-i-1. To be cautious that (-1) provide the last aside to be 0.
    }
    list=list1;
    return list1;
  }
  public static void print(int[] list){
    for(int i = 0; i < list.length; i++){
      System.out.print(list[i]+" ");
    }
    return;
  }

}