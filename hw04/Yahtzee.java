/* 2/19/2018 TianhengXiang CSE002 hw04 Yahtzee
This programme aims to function as a yahtzee game which user could either roll 5 random numbers of dice or manually type in 5 numbers. Eventually the upper section initial total, upper sectoin total including bonus, lower section total and grand total would be displayed. */
import java.util.Scanner;
public class Yahtzee{
  public static void main(String[] args){
		/* Rolling the dices */
		
    int max = 6;
	  int min = 1;
	  int range = max - min + 1;		
	  int dice1=0;
    int dice2=0;
    int dice3=0;
    int dice4=0;
    int dice5=0;
    Scanner myScanner = new Scanner ( System.in );
		System.out.print("In what way you want to input the numbers of the dice? If selected manually please type 1, if selected randomly please type 0 ");
			int selectionKey = myScanner.nextInt();//accept the input of whether select manually or randomly, 1 is manual, 0 is random
		if (selectionKey == 1){
			System.out.print("Please input the first number your want(Domain from 1~6) ");
			dice1 = myScanner.nextInt();
			if(1 <= dice1&&dice1 <= 6){
				System.out.print("Please input the second number your want(Domain from 1~6) ");
				dice2 = myScanner.nextInt();
				if(1 <= dice2&&dice2 <= 6){
					System.out.print("Please input the third number your want(Domain from 1~6) ");
					dice3 = myScanner.nextInt();
			  	if(1 <= dice3&&dice3 <= 6){
						System.out.print("Please input the fourth number your want(Domain from 1~6) ");
						dice4 = myScanner.nextInt();
						if(1 <= dice4&&dice4 <= 6){
							System.out.print("Please input the fifth number your want(Domain from 1~6) ");
							dice5 = myScanner.nextInt();
							if(1 > dice5||dice5 > 6){
									  System.out.println("Invalid input!");
								}
						}
						else {System.out.println("Invalid input!");}
					}
					else {System.out.println("Invalid input!");}
				}
				else {System.out.println("Invalid input!");}
			}
			else {System.out.println("Invalid input!");}
		}
		else if (selectionKey == 0 ){
	  dice1 = (int) (Math.random()* range) + min;
   	dice2 = (int) (Math.random()* range) + min;
   	dice3 = (int) (Math.random()* range) + min;
   	dice4 = (int) (Math.random()* range) + min;
   	dice5 = (int) (Math.random()* range) + min;
		}
		else {System.out.println("Invalid Input!");}
		
		
		/* Stat for the upper section */
		
		
		/* Initialize sub section values for the upper section */
		int ACES = 0;
		int TWOS = 0;
		int THREES = 0;
		int FOURS = 0;
		int FIVES = 0;
		int SIXES = 0;
		int upperHalfWithoutBonus = 0;
		int upperHalfWithBonus = 0;
		
		/* Counting the number of Aces,Twos,Threes,Fours,Fives,Sixes*/
		if ( dice1 == 1){ ++ACES; }
		if ( dice1 == 2){ ++TWOS; }
		if ( dice1 == 3){ ++THREES; }
		if ( dice1 == 4){ ++FOURS; }
		if ( dice1 == 5){ ++FIVES; }
		if ( dice1 == 6){ ++SIXES; }
		if ( dice2 == 1){ ++ACES; }
		if ( dice2 == 2){ ++TWOS; }
		if ( dice2 == 3){ ++THREES; }
		if ( dice2 == 4){ ++FOURS; }
		if ( dice2 == 5){ ++FIVES; }
		if ( dice2 == 6){ ++SIXES; }
		if ( dice3 == 1){ ++ACES; }
		if ( dice3 == 2){ ++TWOS; }
		if ( dice3 == 3){ ++THREES; }
		if ( dice3 == 4){ ++FOURS; }
		if ( dice3 == 5){ ++FIVES; }
		if ( dice3 == 6){ ++SIXES; }
		if ( dice4 == 1){ ++ACES; }
		if ( dice4 == 2){ ++TWOS; }
		if ( dice4 == 3){ ++THREES; }
		if ( dice4 == 4){ ++FOURS; }
		if ( dice4 == 5){ ++FIVES; }
		if ( dice4 == 6){ ++SIXES; }
		if ( dice5 == 1){ ++ACES; }
		if ( dice5 == 2){ ++TWOS; }
		if ( dice5 == 3){ ++THREES; }
		if ( dice5 == 4){ ++FOURS; }
		if ( dice5 == 5){ ++FIVES; }
		if ( dice5 == 6){ ++SIXES; }
		
		/* Calculate the total score before bonus */
		upperHalfWithoutBonus = ACES*1 + TWOS*2 + THREES*3 + FOURS*4 + FIVES*5 + SIXES*6;
		if(upperHalfWithoutBonus >= 63){ upperHalfWithBonus += 35; }
		else { upperHalfWithBonus = upperHalfWithoutBonus; }
		
		
		/* Stat for the lower section */

		
		/* Initialize the subsection values for the lower section */
		int fullHouse = 0;
		int smStraight = 0;
		int lgStraight = 0;
		int YAHTZEE = 0;
		int threeOfKind = 0;
		int fourOfKind = 0;
		int chance = 0;
		
		/* Determination for 3 of a kind */
		if ( ACES == 3 ) { threeOfKind = 1*3; }
		if ( TWOS == 3 ) { threeOfKind = 2*3; }
		if ( THREES == 3 ) { threeOfKind = 3*3; }
    if ( FOURS == 3 ) { threeOfKind = 4*3; }
		if ( FIVES == 3) { threeOfKind = 5*3; }
		
		/* Determination for 4 of a kind */
		if ( ACES == 4 ) { fourOfKind = 1*4; }
		if ( TWOS == 4 ) { fourOfKind = 2*4; }
		if ( THREES == 4 ) { fourOfKind = 3*4; }
    if ( FOURS == 4 ) { fourOfKind = 4*4; }
		if ( FIVES == 4) { fourOfKind = 5*4; }
		
		/* Determination for YAHTZEE */
		if ( ACES == 5 || TWOS == 5 || THREES == 5 || FOURS == 5 || FIVES ==5 ) { YAHTZEE = 40; }
		
		/* Determination for short straight */
		if ((THREES >= 1)&&(FOURS >= 1)&&((ACES >= 1 && TWOS >= 1) || (TWOS >= 1 && FIVES >= 1) || (FIVES >= 1 && SIXES >= 1))) { smStraight = 30; }

		/* Determination for long straight */
		if ( (ACES == 0 && TWOS != 0 && THREES != 0 && FOURS != 0 && FIVES != 0 && SIXES != 0) || (ACES != 0&& TWOS ==0 && THREES != 0 && FOURS != 0 && FIVES != 0 && SIXES != 0) || (ACES != 0&& TWOS !=0 && THREES == 0 && FOURS != 0 && FIVES != 0 && SIXES != 0) || (ACES != 0&& TWOS !=0 && THREES != 0 && FOURS == 0 && FIVES != 0 && SIXES != 0) || (ACES != 0&& TWOS !=0 && THREES != 0 && FOURS != 0 && FIVES == 0 && SIXES != 0) ||(ACES != 0&& TWOS !=0 && THREES != 0 && FOURS != 0 && FIVES != 0 && SIXES == 0)) { lgStraight = 40; }
		
		/* Adding Chance */
		chance = dice1 + dice2 + dice3 + dice4 + dice5;
		
		/* Adding Full house */
		if (( ACES == 3 || TWOS == 3 || THREES == 3 || FOURS == 3 || FIVES == 3)&&(ACES == 2 || TWOS == 2 || THREES == 2 || FOURS == 2 || FIVES == 2 )) { fullHouse = 25; }
		
	  /* Adding up the lower half section */
		int lowerHalf = fullHouse + smStraight + lgStraight + YAHTZEE + threeOfKind + fourOfKind + chance;
		
	
	/* Adding up the upper and lower half sections */
	int grandTotal = upperHalfWithBonus + lowerHalf;
	
		//System.out.println(dice1+dice2+dice3+dice4+dice5);
		
		
	/* Final display */
	System.out.println("The upper section initial total is " + upperHalfWithoutBonus );//output of upper section inital total 
	System.out.println("The upper section total including bonus is " + upperHalfWithBonus );//output of upper section total including bonus
	System.out.println("The lower section total is " + lowerHalf );//output of lower section total
	System.out.println("The grand total is " + grandTotal );//output of grand total
  }
}
  