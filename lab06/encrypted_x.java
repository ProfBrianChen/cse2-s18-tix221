import java.util.Scanner;
public class encrypted_x{
   			public static void main(String[] args) {
          /* test whether the input is in range and in corret type */
          int input = 0;
          System.out.println("Input a integer between 0 - 100. ");
           while(true) {
                Scanner r = new Scanner(System.in); 
                if(!r.hasNextInt()) {
                System.out.println("Input a integer between 0 - 100. ");
                continue;
              }
              input = r.nextInt();
              if(input<0 || input>100) {
                System.out.println("Input a integer between 0 - 100. ");
                continue;
              }
              break;
              }
          /* Printing the X */
          
          
          for (int i = 1; i < (input+1); i++){   // The outer loop to control the number of the lines the displayed
            for(int j = 1; j < (input+1); j++){   //The inner loop to control output on each line
              if ( j == i || j == (input+1-i)){
              System.out.print(" ");
              }
              else{ System.out.print("*"); }
            }
            System.out.println();
            
            
          }
          
        }
}