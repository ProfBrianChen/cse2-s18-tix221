///Tianheng Xiang 2/3/2018 CSE002 This document is Homework assignment 2, which is to calculate the cost of items bought including the PA sales tax.
public class Arithmetic {
    	// main method required for every Java program
   	public static void main(String[] args) {
      //Number of pairs of pants
      int numPants = 3;
      //Cost per pair of pants
      double pantsPrice = 34.98;
      
      //Number of sweatshirts
      int numShirts = 2;
      //Cost per shirt
      double shirtPrice = 24.99;
      
      //Number of belts
      int numBelts = 1;
      //cost per box of belts
      double beltCost = 33.99;
      
      //the tax rate
      double paSalesTax = 0.06;
      
      double totalCostOfPants; //total cost of pants
      double taxOnPants; //tax of pants
      double totalCostOfShirts; //total cost of shirts
      double taxOnShirts; //tax of shirts
      double totalCostOfBelts; //total cost of belts
      double taxOnBelts; //tax of belts
      double totalCostBeforeTax; //total cost of purchases before tax
      double totalSalesTax; //total sales tax
      double totalTransactionWithTax; // total paid for the transaction including sales tax
      
      totalCostOfPants = numPants*pantsPrice; //calculate the cost of pants by multiply number of pants with price of pants
      taxOnPants = totalCostOfPants*paSalesTax; //calculate the tax paid on pants by multiply the total cost of pants with the sales tax of PA
      totalCostOfShirts = numShirts*shirtPrice; //calculate the cost of shirts by multiply number of shirts with price of shirts
      taxOnShirts = totalCostOfShirts*paSalesTax; //calculate the tax paid on shirts by multiply the total cost of shirts with the sales tax of PA
      totalCostOfBelts = numBelts*beltCost; //calculate the cost of belts by multiply number of belts with price of belts
      taxOnBelts = totalCostOfBelts*paSalesTax; //calculate the tax paid on belts by multiply the total cost of belts with the sales tax of PA
      totalCostBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; //calculate the total cost of purchases before tax by adding the total cost of pants, shirts and belts 
      totalSalesTax = taxOnPants + taxOnShirts + taxOnBelts; //calculate the tax of sales by adding taxes on pants, shirts and belts
      totalTransactionWithTax = totalCostBeforeTax + totalSalesTax; //calculate the total paid for the transaction including sales tax
      
      System.out.println("The total cost of pants is $" + String.format("%.2f",totalCostOfPants)); //display the total cost of pants
      System.out.println("The total cost of shirts is $" + totalCostOfShirts); //display the total cost of shirts
      System.out.println("The total cost of belts is $" + totalCostOfBelts); //display the total cost of belts
      System.out.println("The tax charged on pants is $" + String.format("%.2f",taxOnPants)); //display the sales tax charged on pants
      System.out.println("The tax charged on shirts is $" + String.format("%.2f",taxOnShirts)); //display the sales tax charged on shirts
      System.out.println("The tax charged on belts is $" + String.format("%.2f",taxOnBelts)); //display the sales tax charged on belts
      System.out.println("The total cost of purchases before tax is $" + totalCostBeforeTax); //display the total purchases cost before tax
      System.out.println("The total sales tax is $" + String.format("%.2f",totalSalesTax)); //display the total sales tax
      System.out.println("Total paid for this transaction including sales tax is $" + String.format("%.2f",totalTransactionWithTax)); //display the total paid for this transaction including sales tax
 
    }
}